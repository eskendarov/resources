package ru.eskendarov.resources;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class AppResources extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }
}
